<%-- 
    Document   : login
    Created on : Aug 3, 2017, 4:24:24 PM
    Author     : ASHOK
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">


        <title>SB Admin 2</title>

        <jsp:directive.include file = "includeCss.jsp" />

    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <form:form method="post" commandName="userForm" action="${userActionUrl}">
                                <fieldset>
                                    <div class="form-group">
                                        <form:input class="form-control" type="email" id="email" name="email" path="email" placeholder="E-mail"/>
                                    </div>
                                    <div class="form-group">
                                        <form:input class="form-control" type="password" id="password" name="password" path="password" placeholder="Password" />
                                    </div>
                                    <button id="btnLogin" type="button" class="btn btn-lg btn-success btn-block"> Login </button>
                                </fieldset>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <jsp:directive.include file = "includeJs.jsp" />

        <script type="text/javascript">

            $(document).ready(function () {
                $('#btnLogin').on('click', function () {
//                    var x = {email:$('#email').valueOf(), password:$('#password').valueOf()};
//                    
//                    var object = new Object();
//                    object.email = ('#email').valueOf();
//                    object.password = ("#password").valueOf();
//                    var data = JSON.stringify(object);


//                    $.getJSON("ajax/testAuth", {email: $("#email").val(), password: $("#password").val()}, function (response) {
//
//                        if (response == true) {
//                            window.location.href = "employee";
//                        }
//                        else {
//                            console.log("Not");
//                        }
//
//                    });


                    $.ajax({
                        url: "ajax/userAuthentication",
                        data: {email: $("#email").val(), password: $("#password").val()},
                        success: function (response) {

                        if (response == true) {
                            window.location.href = "employee";
                        }
                        else {
                            console.log("Not");
                        }

                    },
                        timeout: 1000 //in milliseconds
                    });



                });
            });

        </script>

    </body> 

</html>