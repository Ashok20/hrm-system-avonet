<%-- 
    Document   : index
    Created on : Aug 3, 2017, 4:24:09 PM
    Author     : ASHOK
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="com.avn.hrmsystem.controller.employee.EmployeeController"%>
<jsp:include page="includeHeader.jsp" > 
    <jsp:param name="title" value="Employee" />
    <jsp:param name="dashTitle" value="Employees" />
</jsp:include>







<div class="col-lg-12">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Employee Name</th>
                    <th>Status</th>
                    <th>Created Date</th>
                    <th>Last Updated Date</th>
                    <th>Created User</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${employeeList}" var="listItem">            
                    <tr id="tow${listItem.employeeId}">
                        <td id="empId${listItem.employeeId}"><c:out value="${listItem.employeeId}">  </c:out></td>
                        <td id="empName${listItem.employeeId}"><c:out value="${listItem.employeeName}">  </c:out></td>
                        <td id="empStatus${listItem.employeeId}"><c:out value="${listItem.status}">  </c:out></td>
                        <td id="empCDate${listItem.employeeId}"><c:out value="${listItem.createdDate}">  </c:out></td>
                        <td id="empLUDate${listItem.employeeId}"><c:out value="${listItem.lastUpdatedDated}">  </c:out></td>
                        <td id="empCUser${listItem.employeeId}"><c:out value="${listItem.createdUser}">  </c:out></td>
                        <td><center><a id="btnEdit${listItem.employeeId}" onclick="editData('${listItem.employeeId}') " ><i class="fa fa-pencil fa-fw"></i></a></center></td>
                        <td><center><a href="" id="btnDelete${listItem.employeeId}"><i class="fa fa-trash fa-fw"></i></a></center></td>
                        </tr>

                </c:forEach>
            </tbody>
        </table>
    </div>
</div>



<jsp:directive.include file = "includeFooter.jsp" />

