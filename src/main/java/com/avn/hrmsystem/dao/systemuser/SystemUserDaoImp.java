
package com.avn.hrmsystem.dao.systemuser;

import com.avn.hrmsystem.model.login.Login;
import com.avn.hrmsystem.model.systemuser.SystemUser;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ASHOK
 */
@Repository("systemUserDao")
public class SystemUserDaoImp implements SystemUserDao {
    
    @Autowired
    DataSource dataSource;


    @Override
    public int addSystemUser(SystemUser systemUser) throws SQLException {
        
        String sql = "INSERT INTO systemuser (UserId, Status) VALUES (?, ?)";
        int status = new JdbcTemplate(dataSource).update(sql, new Object[]{systemUser.getUserId(), systemUser.getStatus()
        });
        
        return status;
    }

    @Override
    public int updateSystemUser(SystemUser systemUser) throws SQLException {
        
        String sql = "UPDATE systemuser SET UserId=?, Status=? WHERE ID=?";
        int status = new JdbcTemplate(dataSource).update(sql, new Object[]{systemUser.getUserId(), systemUser.getStatus()
        });
        
        return status;
    }

    @Override
    public int deleteSystemUser(int input) throws SQLException {
        
        String sql = "DELETE FROM systemuser WHERE ID=?";
        int status = new JdbcTemplate(dataSource).update(sql, input);        
        return status;
    }
    

    @Override
    public List<SystemUser> getSystemUser(Login input) throws SQLException {
        
        List<SystemUser> sUserList = new JdbcTemplate(dataSource).query(
        "SELECT * FROM systemuser WHERE email=? AND password=?",
        new RowMapper<SystemUser>() {
            public SystemUser mapRow(ResultSet rs, int rowNum) throws SQLException {                
                SystemUser sysUser = new SystemUser();
                sysUser.setId(rs.getInt("ID"));
                sysUser.setUserId(rs.getInt("UserID"));
                sysUser.setEmail(rs.getString("email"));
                sysUser.setStatus(rs.getInt("Status"));
                return sysUser;
            }
        }, input.getEmail(), input.getPassword());
        
        return sUserList;
    
    }
    
}
