
package com.avn.hrmsystem.dao.userrole;

import com.avn.hrmsystem.model.userrole.UserRole;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ASHOK
 */
@Repository("userRoleDao")
public class UserRoleDaoImp implements UserRoleDao {
    
    
    @Autowired
    DataSource dataSource;

    @Override
    public int addUserRole(UserRole userRole) throws SQLException {
        String sql = "INSERT INTO userrole (Description, Status) VALUES (?, ?)";
        int status = new JdbcTemplate(dataSource).update(sql, new Object[]{userRole.getDescription(), userRole.getStatus()
        });
        
        return status;
    }

    @Override
    public int updateUserRole(UserRole userRole) throws SQLException {
        String sql = "UPDATE userrole SET Description=?, Status=? WHERE UserRoleID=?";
        int status = new JdbcTemplate(dataSource).update(sql, new Object[]{userRole.getDescription(), userRole.getStatus(), userRole.getUserRoleId()
        });
        
        return status;
    }

    @Override
    public int deleteUserRole(int input) throws SQLException {
        String sql = "DELETE FROM userrole WHERE UserRoleID=?";
        int status = new JdbcTemplate(dataSource).update(sql, input);        
        return status;
    }

    
}
