
package com.avn.hrmsystem.dao.employee;

import com.avn.hrmsystem.model.employee.Employee;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author ASHOK
 */
public interface EmployeeDao {
    
    public int addEmployee(Employee employee) throws SQLException;
    
    public int updateEmployee(Employee employee)throws SQLException;
    
    public int deleteEmployee(int input)throws SQLException;
    
    public List<Employee> getEmployees()throws SQLException;

}
