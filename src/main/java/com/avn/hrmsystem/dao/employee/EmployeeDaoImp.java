
package com.avn.hrmsystem.dao.employee;

import com.avn.hrmsystem.model.employee.Employee;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ASHOK
 */
@Repository("employeeDao")
public class EmployeeDaoImp implements EmployeeDao {
    
    
    @Autowired
    DataSource dataSource;
    
//    @Autowired
//    JdbcTemplate jdbcTemplate;   

    @Override
    public int addEmployee(Employee employee) throws SQLException {

        String sql = "INSERT INTO employee (EmployeeName, Status, CreatedDate, LastUpdatedDate, CreatedUser) VALUES (?, ?, ?, ?, ?)";

        int status = new JdbcTemplate(dataSource).update(sql, new Object[]{employee.getEmployeeName(), employee.getStatus(),
            employee.getCreatedDate(), employee.getLastUpdatedDated(), employee.getCreatedUser()
        });
        
        return status;
    }
    
    @Override
    public int updateEmployee(Employee employee) throws SQLException {
        
        String sql = "UPDATE employee SET EmployeeName=?, Status=?, CreatedDate=?, LastUpdatedDate=?, CreatedUser=? WHERE employeeid=?";

        int status = new JdbcTemplate(dataSource).update(sql, new Object[]{employee.getEmployeeName(), employee.getStatus(),
            employee.getCreatedDate(), employee.getLastUpdatedDated(), employee.getCreatedUser()
        });
        
        return status;
    }
    
    @Override
    public int deleteEmployee(int input) throws SQLException {
        
        String sql = "DELETE FROM employee WHERE employeeid=?";
        int status = new JdbcTemplate(dataSource).update(sql, input);        
        return status;
    }    

    @Override
    public List<Employee> getEmployees() throws SQLException {
        
        List<Employee> empArry = new JdbcTemplate(dataSource).query(
        "SELECT * FROM employee",
        new RowMapper<Employee>() {
            public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {                
                Employee emp = new Employee();
                emp.setEmployeeId(rs.getInt("EmployeeID"));
                emp.setEmployeeName(rs.getString("EmployeeName"));
                emp.setStatus(rs.getInt("Status"));
                emp.setCreatedDate(rs.getString("CreatedDate"));
                emp.setLastUpdatedDated(rs.getString("LastUpdatedDate"));
                emp.setCreatedUser(rs.getInt("CreatedUser"));
                return emp;
            }
        });
        
        return empArry;
    }

}
