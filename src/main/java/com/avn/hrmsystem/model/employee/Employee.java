
package com.avn.hrmsystem.model.employee;

/**
 *
 * @author ASHOK
 */
public class Employee {
    
    protected int employeeId;
    protected String employeeName;
    protected int status;
    protected String createdDate;
    protected String lastUpdatedDated;
    protected int createdUser;


    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastUpdatedDated() {
        return lastUpdatedDated;
    }

    public void setLastUpdatedDated(String lastUpdatedDated) {
        this.lastUpdatedDated = lastUpdatedDated;
    }

    public int getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(int createdUser) {
        this.createdUser = createdUser;
    }

    
}
