
package com.avn.hrmsystem.model.userrole;

/**
 *
 * @author ASHOK
 */
public class UserRole {
    protected int userRoleId;
    protected String description;
    protected int status;
    
    
    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
}
