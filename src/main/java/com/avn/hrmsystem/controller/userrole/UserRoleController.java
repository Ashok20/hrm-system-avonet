package com.avn.hrmsystem.controller.userrole;

import com.avn.hrmsystem.model.userrole.UserRole;
import com.avn.hrmsystem.service.userrole.UserRoleService;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author ASHOK
 */
@Controller
public class UserRoleController {

    @Autowired
    UserRoleService userRoleSerivce;

    Scanner sc = new Scanner(System.in);
    
    
    @RequestMapping(value = "/userrole", method = RequestMethod.GET)
    public String loadUserRole() {

        return "userRole";
    }
    
    

    public int addUserRole() {

        UserRole userRole = new UserRole();

        System.out.println("Enter User Role Description : ");
        userRole.setDescription(sc.next());

        System.out.println("Enter UserRole Status : ");
        userRole.setStatus(Integer.parseInt(sc.next()));

        int status = 0;
        try {
            status = userRoleSerivce.addUserRole(userRole);
        } catch (SQLException ex) {
            Logger.getLogger(UserRoleController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return status;
    }

    public int updateUserRole() {

        UserRole userRole = new UserRole();

        System.out.println("Enter User Role Description : ");
        userRole.setDescription(sc.next());

        System.out.println("Enter UserRole Status : ");
        userRole.setStatus(Integer.parseInt(sc.next()));

        int status = 0;
        try {
            status = userRoleSerivce.updateUserRole(userRole);
        } catch (SQLException ex) {
            Logger.getLogger(UserRoleController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return status;
    }

    public int deleteUserRole() {

        System.out.println("Enter User Role ID : ");
        int userRoleId = Integer.parseInt(sc.next());

        System.out.println("Are you sure you want to delete thie user role ? (Y/N)");
        String confirm = sc.next();

        if (confirm.equals("Y") || confirm.equals("y")) {

            int status = 0;
            try {
                status = userRoleSerivce.deleteUserRole(userRoleId);
            } catch (SQLException ex) {
                Logger.getLogger(UserRoleController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return status;
        } else {
            return -1;
        }
    }

}
