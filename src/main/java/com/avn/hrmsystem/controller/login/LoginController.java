package com.avn.hrmsystem.controller.login;

import com.avn.hrmsystem.model.login.Login;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author ASHOK
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loadLogin(@ModelAttribute("userForm") Login login) {

        return "login";
    }   
}

