package com.avn.hrmsystem.controller.ajax;

import com.avn.hrmsystem.model.login.Login;
import com.avn.hrmsystem.service.systemuser.SystemUserService;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/ajax")
public class AjaxHandler {

    @Autowired
    SystemUserService systemUserService;

    @RequestMapping(value = "/userAuthentication", method = RequestMethod.GET)
    public @ResponseBody boolean loginAuthentication(HttpServletRequest request) {

        Login login = new Login();
        login.setEmail(request.getParameter("email"));
        login.setPassword(request.getParameter("password"));

        List sUserList = null;

        try {
            sUserList = systemUserService.getSystemUser(login);
        } catch (SQLException | ClassCastException ex) {
            Logger.getLogger(AjaxHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (sUserList.size() == 1) {
            return true;
        } else {
            return false;
        }
    }

}
