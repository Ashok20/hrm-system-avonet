
package com.avn.hrmsystem.controller.systemuser;

import com.avn.hrmsystem.model.systemuser.SystemUser;
import com.avn.hrmsystem.service.systemuser.SystemUserService;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author ASHOK
 */
@Controller
public class SystemUserController {
    
    @Autowired
    SystemUserService systemUserService;

    Scanner sc = new Scanner(System.in);

    public int addSystemUser() {

        SystemUser systemUser = new SystemUser();

        System.out.println("Enter Employee Id : ");
        systemUser.setUserId(Integer.parseInt(sc.next()));

        System.out.println("Enter System User Status : ");
        systemUser.setStatus(Integer.parseInt(sc.next()));

        int status = 0;
        try {
            status = systemUserService.addSystemUser(systemUser);
        } catch (SQLException ex) {
            Logger.getLogger(SystemUserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return status;
    }

    public int updateUserRole() {

        SystemUser systemUser = new SystemUser();

        System.out.println("Enter Employee Id : ");
        systemUser.setUserId(Integer.parseInt(sc.next()));

        System.out.println("Enter System User Status : ");
        systemUser.setStatus(Integer.parseInt(sc.next()));

        int status = 0;
        try {
            status = systemUserService.updateSystemUser(systemUser);
        } catch (SQLException ex) {
            Logger.getLogger(SystemUserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return status;
    }

    public int deleteUserRole() {

        System.out.println("Enter System User ID : ");
        int systemUserId = Integer.parseInt(sc.next());

        System.out.println("Are you sure you want to delete thie system user ? (Y/N)");
        String confirm = sc.next();

        if (confirm.equals("Y") || confirm.equals("y")) {

            int status = 0;
            try {
                status = systemUserService.deleteSystemUser(systemUserId);
            } catch (SQLException ex) {
                Logger.getLogger(SystemUserController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return status;
        } else {
            return -1;
        }
    }
   
}
