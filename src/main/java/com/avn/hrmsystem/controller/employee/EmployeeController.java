package com.avn.hrmsystem.controller.employee;

import com.avn.hrmsystem.model.employee.Employee;
import com.avn.hrmsystem.model.login.Login;
import com.avn.hrmsystem.service.employee.EmployeeServiceImp;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author ASHOK
 */
@Controller
public class EmployeeController {
    
    @Autowired
    EmployeeServiceImp empService;
    
    
    
    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public String loadEmployee(@ModelAttribute("userForm") Login login, ModelMap model) {
        
        List<Employee> empList = getEmployees();        
        model.put("employeeList" , empList);        
        
        return "index";
    }

    
    Scanner sc = new Scanner(System.in);
    
    public int addEmployee() {
        
        Employee emp = new Employee();
        
        System.out.println("Enter Employee Name : ");
        emp.setEmployeeName(sc.next());
        
        System.out.println("Enter Employee Status : ");
        emp.setStatus(Integer.parseInt(sc.next()));
        
        System.out.println("Date : ");
        emp.setCreatedDate(sc.next());
        
        System.out.println("Last Update Date : ");
        emp.setLastUpdatedDated(sc.next());
        
        System.out.println("Created User Id : ");
        emp.setCreatedUser(Integer.parseInt(sc.next()));
        
        int status = 0;
        try {
            status = empService.addEmployee(emp);
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }
    
    public int updateEmployee() {
        
        Employee emp = new Employee();
        
        System.out.println("Enter Employee Name : ");
        emp.setEmployeeName(sc.next());
        
        System.out.println("Enter Employee Status : ");
        emp.setStatus(Integer.parseInt(sc.next()));
        
        System.out.println("Date : ");
        emp.setCreatedDate(sc.next());
        
        System.out.println("Last Update Date : ");
        emp.setLastUpdatedDated(sc.next());
        
        System.out.println("Created User Id : ");
        emp.setCreatedUser(Integer.parseInt(sc.next()));
        
        int status = 0;
        try {
            status = empService.updateEmployee(emp);
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }

    
    public int deleteEmployee() {

        System.out.println("Enter Employee ID : ");
        int employeeId = Integer.parseInt(sc.next());

        System.out.println("Are you sure you want to delete this employee ? (Y/N)");
        String confirm = sc.next();

        if (confirm.equals("Y") || confirm.equals("y")) {

            int status = 0;
            try {
                status = empService.deleteEmployee(employeeId);
            } catch (SQLException ex) {
                Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return status;
        } else {
            return -1;
        }
    }
    
    
    public List<Employee> getEmployees(){
        
        List<Employee> empArray = null;
        
        try {
            empArray = empService.getEmployees();
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empArray;
    }
    
    
    
    
}
