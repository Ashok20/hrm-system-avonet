package com.avn.hrmsystem.service.employee;

import com.avn.hrmsystem.dao.employee.EmployeeDao;
import com.avn.hrmsystem.model.employee.Employee;
import com.avn.hrmsystem.service.systemuser.SystemUserServiceImp;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASHOK
 */
@Service("employeeService")
public class EmployeeServiceImp implements EmployeeService {

    @Autowired
    EmployeeDao empdao;
    
    @Autowired
    SystemUserServiceImp systemUserImp;
    
    
    @Override
    @Transactional
    public int addEmployee(Employee employee) throws SQLException {
        int employeeStatus = empdao.addEmployee(employee);        
        int systemUserStatus = systemUserImp.addSystemUser(employee);
        return employeeStatus;
    }

    @Override
    @Transactional
    public int updateEmployee(Employee employee) throws SQLException {
        return empdao.updateEmployee(employee);
    }

    @Override
    @Transactional
    public int deleteEmployee(int input) throws SQLException {         
        return empdao.deleteEmployee(input);
    }

    @Override
    public List getEmployees() throws SQLException {
        return empdao.getEmployees();
    }

}
