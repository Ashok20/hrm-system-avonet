package com.avn.hrmsystem.service.systemuser;

import com.avn.hrmsystem.dao.systemuser.SystemUserDao;
import com.avn.hrmsystem.model.employee.Employee;
import com.avn.hrmsystem.model.login.Login;
import com.avn.hrmsystem.model.systemuser.SystemUser;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASHOK
 */
@Service("systemUserService")
public class SystemUserServiceImp implements SystemUserService {

    @Autowired
    SystemUserDao systemuserdao;

    @Override
    @Transactional
    public int addSystemUser(Employee input) throws SQLException {
        SystemUser systemUser = new SystemUser();
        systemUser.setStatus(input.getStatus());
        systemUser.setUserId(input.getEmployeeId());
        return systemuserdao.addSystemUser(systemUser);
    }

    @Override
    @Transactional
    public int addSystemUser(SystemUser systemUser) throws SQLException {
        return systemuserdao.addSystemUser(systemUser);
    }

    @Override
    @Transactional
    public int updateSystemUser(SystemUser systemUser) throws SQLException {
        return systemuserdao.updateSystemUser(systemUser);
    }

    @Override
    @Transactional
    public int deleteSystemUser(int input) throws SQLException {
        return systemuserdao.deleteSystemUser(input);
    }

    @Override
    @Transactional
    public List<SystemUser> getSystemUser(Login input) throws SQLException {
        return systemuserdao.getSystemUser(input);
    }

}
