
package com.avn.hrmsystem.service.systemuser;

import com.avn.hrmsystem.model.employee.Employee;
import com.avn.hrmsystem.model.login.Login;
import com.avn.hrmsystem.model.systemuser.SystemUser;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author ASHOK
 */
public interface SystemUserService {
    
    public int addSystemUser(Employee employee)throws SQLException;
    
    public int addSystemUser(SystemUser systemUser)throws SQLException;
    
    public int updateSystemUser(SystemUser systemUser) throws SQLException;
    
    public int deleteSystemUser(int input) throws SQLException;
    
    public List<SystemUser> getSystemUser(Login input) throws SQLException;
    
}
