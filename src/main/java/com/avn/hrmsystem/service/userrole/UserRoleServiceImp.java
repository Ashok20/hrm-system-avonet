
package com.avn.hrmsystem.service.userrole;

import com.avn.hrmsystem.dao.userrole.UserRoleDao;
import com.avn.hrmsystem.model.userrole.UserRole;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ASHOK
 */
@Service("userRoleService")
public class UserRoleServiceImp implements UserRoleService{

    @Autowired
    UserRoleDao userRoleDao;
    
    @Override
    public int addUserRole(UserRole userRole) throws SQLException {
        return userRoleDao.addUserRole(userRole);
    }

    @Override
    public int updateUserRole(UserRole userRole) throws SQLException {
        return userRoleDao.updateUserRole(userRole);
    }

    @Override
    public int deleteUserRole(int input) throws SQLException {
        return userRoleDao.deleteUserRole(input);
    }
    
    
}
