
package com.avn.hrmsystem.service.userrole;

import com.avn.hrmsystem.model.userrole.UserRole;
import java.sql.SQLException;

/**
 *
 * @author ASHOK
 */
public interface UserRoleService {
    
    public int addUserRole(UserRole userRole)throws SQLException;
    
    public int updateUserRole(UserRole userRole)throws SQLException;
    
    public int deleteUserRole(int input)throws SQLException;
}
