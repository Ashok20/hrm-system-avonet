<%-- 
    Document   : includeCss
    Created on : Aug 4, 2017, 4:56:17 PM
    Author     : ASHOK
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- Bootstrap Core CSS -->
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootCss" />
<link href="${bootCss}" rel="stylesheet" type="text/css"/>


<!-- MetisMenu CSS -->
<spring:url value="/resources/core/css/metisMenu.min.css" var="metisMenuCss" />
<link href="${metisMenuCss}" rel="stylesheet" type="text/css"/>


<!-- Custom CSS -->
<spring:url value="/resources/core/css/sb-admin-2.min.css" var="sbAdmin2Css" />
<link href="${sbAdmin2Css}" rel="stylesheet" type="text/css"/>


<!-- Custom Fonts -->
<spring:url value="/resources/core/font/fonts/css/font-awesome.min.css" var="fontAwesomeCss" />
<link href="${fontAwesomeCss}" rel="stylesheet" type="text/css"/>