<%-- 
    Document   : includeJs
    Created on : Aug 4, 2017, 4:56:51 PM
    Author     : ASHOK
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!-- jQuery -->
<spring:url value="/resources/core/js/jquery-3.2.1.min.js" var="jQueryJs" />
<script src="${jQueryJs}" type="text/javascript"></script>


<!-- DashBoard Body Custom JavaScript -->
<spring:url value="/resources/core/js/dashCustom.js" var="dashCustomJs" />
<script src="${dashCustomJs}" type="text/javascript"></script>


<!-- Bootstrap Core JavaScript -->
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
<script src="${bootstrapJs}" type="text/javascript"></script>


<!-- Metis Menu Plugin JavaScript -->
<spring:url value="/resources/core/js/metisMenu.min.js" var="metisMenuJs" />
<script src="${metisMenuJs}" type="text/javascript"></script>


<!-- Custom Theme JavaScript -->
<spring:url value="/resources/core/js/sb-admin-2.js" var="sbAdmin2Js" />
<script src="${sbAdmin2Js}" type="text/javascript"></script>


